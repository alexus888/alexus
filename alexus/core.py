import functools
import time


def timer(func):
    """Print the runtime of the decorated function."""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer


def pdir(object, search=None, show_private=False):
    """Print and return all objects in a `dir` call."""
    all_objects = dir(object)

    if show_private == False:
        all_objects = [obj for obj in all_objects if obj[0] != '_']
    
    if search:
        all_objects = [obj for obj in all_objects if search in obj]

    if len(all_objects) == 0:
        output = ["\nNo objects found.\n"]
    else:
        max_width = max([len(obj) for obj in all_objects])

        def _format(str1, str2, width: int) -> str:
            return '\t' + str1.ljust(width + 5, '.') + str2

        output = [_format(obj, str(type(getattr(object, obj))), max_width) for obj in all_objects]
    
    for out in output:
        print(out)


def debug(func):
    """Print the function signature and return value"""
    @functools.wraps(func)
    def wrapper_debug(*args, **kwargs):
        args_repr = [repr(a) for a in args]                      # 1
        kwargs_repr = [f"{k}={v!r}" for k, v in kwargs.items()]  # 2
        signature = ", ".join(args_repr + kwargs_repr)           # 3
        print(f"Calling {func.__name__}({signature})")
        value = func(*args, **kwargs)
        print(f"{func.__name__!r} returned {value!r}")           # 4
        return value
    return wrapper_debug


def keyboard():
    import readline # optional, will allow Up/Down/History in the console
    import code
    variables = globals().copy()
    variables.update(locals())
    shell = code.InteractiveConsole(variables)
    shell.interact()